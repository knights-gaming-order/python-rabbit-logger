from setuptools import setup, find_packages

setup(name="rabbit-logger",
    version="0.1.0",
    description="Helpers for logging using RabbitMQ",
    url="https://gitlab.com/knights-gaming-order/python-rabbit-logger",
    author="Clifford R.",
    author_email="cmrallen@gmail.com",
    license="MIT",
    packages=find_packages(),
    install_requires=[
        'pika==0.11.2',
    ],
    zip_safe=False
)
