"""
This module contains the Logger class
which contains the methods for logging
"""
from typing import Any
import json
from functools import partial
from pika.connection import Connection
from pika.spec import BasicProperties

DEBUG = 4
INFO = 3
WARN = 2
ERROR = 1
FATAL = 0

class Config:
    def __init__(self, **kwargs):
        self.json_class = kwargs.get("json_class")
        self.level = kwargs.get("level")

class Logger:
    """
    Logger class.
    """
    _name = None
    _publish_channel = None

    level = 3

    @property
    def json_dumps(self):
        return json.dumps

    def __init__(self, rabbit_conn, logger_config=None):
        if logger_config is None:
            logger_config = Config()

        if logger_config.json_class:
            self.json_dumps = partial(json.dumps, cls=json_class)

        if isinstance(logger_config.level, int):
            self.level = logger_config.level

        if not isinstance(rabbit_conn, Connection):
            raise ValueError("rabbit_conn must be a valid RabbitMQ connection")

        self._publish_channel = None
        self._publish_properties = BasicProperties(content_type="application/json")


    def _publish(self, level, data):
        self._publish_channel.basic_publish(
            "logs",
            level,
            data,
            self._publish_properties,
            False,
            False
        )

    def debug(self, message:str, **kwargs):
        if self.level < DEBUG:
            return
        data = {
            "message": message,
            "data": kwargs
        }
        self._publish("debug", self.json_dumps(data))

    def info(self, message:str, **kwargs):
        if self.level < INFO:
            return
        data = {
            "message": message,
            "data": kwargs
        }
        self._publish("info", self.json_dumps(data))

    def warn(self, message:str, **kwargs):
        if self.level < WARN:
            return
        data = {
            "message": message,
            "data": kwargs
        }
        self._publish("warn", self.json_dumps(data))

    def error(self, message:str, **kwargs):
        if self.level < ERROR:
            return
        data = {
            "message": message,
            "data": kwargs
        }
        self._publish("error", self.json_dumps(data))

    def fatal(self, message:str, **kwargs):
        if self.level < FATAL:
            return
        data = {
            "message": message,
            "data": kwargs
        }
        self._publish("fatal", self.json_dumps(data))
