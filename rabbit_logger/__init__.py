"""
This package is for retrieving a logger that
communicates with RabbitMQ to send logs to the
lancebot logging service.
"""
import os
from pika import ConnectionParameters
from pika.exceptions import ConnectionClosed
from pika.adapters.blocking_connection import BlockingConnection
from rabbit_logger.logger import Logger


_LOGGERS = {}


def _get_default_conn():
    params = ConnectionParameters(host=os.getenv("RABBITMQ_URL", "localhost"))
    return BlockingConnection(parameters=params)



def get_default_logger(name=None):
    if name not in _LOGGERS:
        _LOGGERS[name] = Logger(_get_default_conn())
    return _LOGGERS[name]
