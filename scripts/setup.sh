#!/bin/bash

virtualenv -p python3 .venv
source $(pwd)/.venv/bin/activate
pip install -r requirements/dev.txt
python setup.py develop
