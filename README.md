# Python rabbit logger

This is the python library helper for the rabbit mq logging

## how to use

Just setup the library using `python setup.py develop` if you wish to manually install. Otherwise if you want to develop on it you should just use `make setup` to facilitate things for yourself.
