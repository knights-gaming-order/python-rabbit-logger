"""
Tests the config for the logger.
"""
import json
import pytest
from rabbit_logger.logger import Config, Logger

class TestLogger(Logger):
    set_level = None
    message = "test"

    def __init__(self):
        pass

    def _publish(self, level, data):
        assert self.set_level == level
        assert self.message == json.loads(data)['message']


def test_default_config_values():
    config = Config()
    assert config.level == None
    assert config.json_class == None


def test_init_fails_without_valid_connection():
    with pytest.raises(ValueError):
        Logger(None)


def test_levels_are_correctly_passed():
    logger = TestLogger()

    logger.set_level = "debug"
    logger.debug(logger.message)

    logger.set_level = "info"
    logger.info(logger.message)

    logger.set_level = "warn"
    logger.warn(logger.message)

    logger.set_level = "error"
    logger.error(logger.message)

    logger.set_level = "fatal"
    logger.fatal(logger.message)
